// importing modules
require("dotenv").config();
const cors = require("cors");
const express = require("express");
const helmet = require("helmet");
const morgan = require("morgan");
const routes = require("./app/routes/app.routes");
const app = express();

// initialize express app use and set package.json
app.use(
  express.json(),
  express.urlencoded({ extended: false }), // for parsing application/x-www-form-urlencoded
  express.static(__dirname + "/public"), // set the static files location /public/img will be /img for users
  helmet(), // adding Helmet to enhance API's security
  cors({ origin: "http://localhost:3000" }), // enabling CORS for all requests
  morgan("combined"), // adding morgan to log HTTP requests
  routes // set routes
);

// listen to server
const PORT = process.env.PORT || 3000; // set port
app.listen(PORT, () => {
  console.log(`Server is running on port: ${PORT}`);
  console.log(`Server Time: ${Date().toString()}`);
});