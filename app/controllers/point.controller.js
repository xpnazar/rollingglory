const points = require("../models/point.model");

exports.check = async (req, res, next) => {
  // call check function from point model
  points.check(res, req.params.id, next);
};

exports.create = (req, res, next) => {
  // create temp variable to store data
  const data = { ...req.body };

  // call create function from point model
  points.create(res, data, next);
};
