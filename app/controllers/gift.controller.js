const moment = require("moment");
const gifts = require("../models/gift.model");

exports.findAll = async (req, res, next) => {
  // call findAll function from gift model
  gifts.findAll(req, res, next);
};

exports.findOne = async (req, res, next) => {
  // call findOne function from gift model
  gifts.findOne(res, req.params.id, next);
};

exports.create = (req, res, next) => {
  // create temp variable to store data
  const data = { ...req.body };

  // call create function from gift model
  gifts.create(res, data, next);
};

exports.update = (req, res, next) => {
  // create temp variable to store data
  let time = moment().format("YYYY-MM-DD HH:mm:ss");
  const data = { ...req.body, updated_at: time };

  // call update function from gift model
  gifts.update(res, req.params.id, data, next);
};

exports.updateAttribute = (req, res, next) => {
  // create temp variable to store data
  let time = moment().format("YYYY-MM-DD HH:mm:ss");
  const data = { ...req.body, updated_at: time };

  // call update function from gift model
  gifts.updateAttribute(res, req.params.id, data, next);
};

exports.delete = (req, res, next) => {
  // call delete function from gift model
  gifts.delete(res, req.params.id, next);
};

exports.redeem = (req, res, next) => {
  // redeem temp variable to store data
  const data = { ...req.body, userID: req.params.id };

  // call redeem function from gift model
  gifts.redeem(res, data, next);
};

exports.redeemByUser = (req, res, next) => {
  // call redeemByUser function from gift model
  gifts.redeemByUser(res, req.params.id, next);
};

exports.rating = (req, res, next) => {
  // rating temp variable to store data
  const data = { ...req.body, userID: req.params.id };

  // call rating function from gift model
  gifts.rating(res, data, next);
};
