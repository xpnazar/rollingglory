const moment = require("moment");
const users = require("../models/user.model");

exports.register = (req, res, next) => {
  // create temp variable to store data
  const data = { ...req.body, authorization: req.headers.authorization.split(" ")[1] };

  // call register function from user model
  users.register(res, data, next);
};

exports.login = (req, res, next) => {
  // create temp variable to store data
  const data = { ...req.body };

  // call login function from user model
  users.login(res, data, next);
};

exports.userData = async (req, res, next) => {
  // create temp variable to store data
  const data = { ...req.headers };

  // call userData function from user model
  users.userData(res, data, next);
};

exports.allUser = async (req, res, next) => {
  // call allUser function from user model
  users.allUser(res, next);
};

exports.update = (req, res, next) => {
  // create temp variable to store data
  let time = moment().format("YYYY-MM-DD HH:mm:ss");
  const data = { ...req.body, updated_at: time };

  // call update function from user model
  users.update(res, req.params.id, data, next);
};

exports.changePassword = (req, res, next) => {
  // create temp variable to store data
  let time = moment().format("YYYY-MM-DD HH:mm:ss");
  const data = { ...req.body, updated_at: time };

  // call update function from user model
  users.changePassword(res, req.params.id, data, next);
};

exports.delete = (req, res, next) => {
  // call delete function from user model
  users.delete(res, req.params.id, next);
};
