const conn = require("../config/db.config").promise();

// check user point
exports.check = async (res, id, next) => {
  try {
    // check if user is exists
    const [checkUser] = await conn.query(
      "SELECT `id`, `name` FROM `tbl_user` WHERE `id`= ?",
      [id]
    );
    if (checkUser.length === 0) {
      return res.status(422).json({
        success: false,
        message: "user not found",
      });
    }

    // if user exist, count user point
    const [plus] = await conn.query(
      "SELECT SUM(point) AS point FROM `tbl_point` WHERE user_id = ? AND 	gift_id IS NULL",
      [id]
    );
    const [min] = await conn.query(
      "SELECT SUM(point) AS point FROM `tbl_point` WHERE user_id = ? AND 	gift_id IS NOT NULL",
      [id]
    );

    if (plus != null || min != null) {
      return res.json({
        success: true,
        data: {
          id: checkUser[0].id,
          name: checkUser[0].name,
          point: plus[0].point - min[0].point,
        },
      });
    } else {
      res.status(422).json({
        success: false,
        message: "user not found",
      });
    }
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// plus or minus point to user
exports.create = async (res, data, next) => {
  try {
    // check if user is exists
    const [checkUser] = await conn.query(
      "SELECT `id` FROM `tbl_user` WHERE `id`= ?",
      [data.user_id]
    );
    if (checkUser.length === 0) {
      return res.status(422).json({
        success: false,
        message: "user not found",
      });
    }

    // if user exist, plus/minus point
    const [create] = await conn.query(
      "INSERT INTO `tbl_point`(`user_id`,`point`) VALUES(?,?)",
      [data.user_id, data.point]
    );
    if (create.affectedRows === 1) {
      return res.status(201).json({
        success: true,
        message: "point saved successfully",
      });
    }
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};
