require("dotenv").config();
const base64ToImage = require("base64-to-image");
const moment = require("moment");
const crypto = require("crypto");
const fs = require("fs");
const conn = require("../config/db.config").promise();

// view all gift data
exports.findAll = async (req, res, next) => {
  try {
    let page = req.query["page"] - 1; // pagination
    let perPage = req.query["perPage"]; // data per page
    const [query] = await conn.query(
      `SELECT * FROM tbl_gift LIMIT ${page * perPage}, ${perPage}`
    );

    if (query.length > 0) {
      // change date formate from 2022-03-03T15:15:45.000Z to 2022-03-03 15:15:45
      var data = [];
      for (const value of query) {
        // check stock
        const [stock] = await conn.query(
          "SELECT * FROM `tbl_redeem` WHERE `gift_id`= ?",
          [value.id]
        );
        let stockLeft = value.stock - stock.length;

        // count rating
        const [rating] = await conn.query(
          "SELECT a.rating FROM tbl_rating as a LEFT JOIN tbl_redeem as b ON ( a.redeem_id = b.id ) WHERE b.gift_id = ?",
          [value.id]
        );
        let overallRating = 0;
        if (rating.length > 0) {
          overallRating =
            rating.reduce((a, b) => a + b.rating, 0) / rating.length;
        }
        let status =
          value.status == 1
            ? "Hot Items"
            : value.status == 2
            ? "New"
            : value.status == 2
            ? "Best Seller"
            : null;
        data.push({
          id: value.id,
          code: value.code,
          name: value.name,
          color: value.color,
          ram: value.ram,
          storage: value.storage,
          point: value.point,
          totalStock: value.stock,
          availableStock: stockLeft,
          inStock: stockLeft > 0 ? true : false,
          totalReview: rating.length,
          rating: (overallRating * 2).toFixed() / 2,
          statusCode: value.status,
          status: status,
          image: value.image == "" ? null : `http://${process.env.SERVER}:${process.env.PORT}/${process.env.GIFT_PATH}${value.image}`,
          description: value.description,
          createdAt: moment(value.created_at).format("YYYY-MM-DD HH:mm:ss"),
          updatedAt:
            value.updated_at == null
              ? null
              : moment(value.updated_at).format("YYYY-MM-DD HH:mm:ss"),
        });
      }

      // sort data by date or rating
      let orderBy =
        req.query["orderBy"] == "rating" ? "rating" : req.query["orderBy"]; // by input date or by rating
      if (orderBy == "rating") {
        // order by rating -> ASC
        data.sort((a, b) =>
          a.rating > b.rating ? 1 : b.rating > a.rating ? -1 : 0
        );
      } else {
        // order by tanggal input -> ASC
        data.sort((a, b) =>
          a.createdAt > b.createdAt ? 1 : b.createdAt > a.createdAt ? -1 : 0
        );
      }

      // if orderBy is DESC, then reverse the array
      let order = req.query["order"].toLowerCase(); // ASC or DESC
      if (order == "desc") {
        data.reverse();
      }

      return res.json({
        success: true,
        data: data,
      });
    } else {
      return res.status(422).json({
        success: false,
        message: "data not found",
      });
    }
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// find by id
exports.findOne = async (res, id, next) => {
  try {
    const [query] = await conn.query("SELECT * FROM `tbl_gift` WHERE `id`= ?", [
      id,
    ]);

    if (query.length > 0) {
      // change date formate from 2022-03-03T15:15:45.000Z to 2022-03-03 15:15:45
      var data = [];
      for (const value of query) {
        // check stock
        const [stock] = await conn.query(
          "SELECT * FROM `tbl_redeem` WHERE `gift_id`= ?",
          [value.id]
        );
        let stockLeft = value.stock - stock.length;

        // count rating
        const [rating] = await conn.query(
          "SELECT a.rating FROM tbl_rating as a LEFT JOIN tbl_redeem as b ON ( a.redeem_id = b.id ) WHERE b.gift_id = ?",
          [value.id]
        );
        let overallRating = 0;
        if (rating.length > 0) {
          overallRating =
            rating.reduce((a, b) => a + b.rating, 0) / rating.length;
        }
        let status =
          value.status == 1
            ? "Hot Items"
            : value.status == 2
            ? "New"
            : value.status == 2
            ? "Best Seller"
            : null;
        data.push({
          id: value.id,
          code: value.code,
          name: value.name,
          color: value.color,
          ram: value.ram,
          storage: value.storage,
          point: value.point,
          totalStock: value.stock,
          availableStock: stockLeft,
          inStock: stockLeft > 0 ? true : false,
          totalReview: rating.length,
          rating: (overallRating * 2).toFixed() / 2,
          statusCode: value.status,
          status: status,
          image: value.image == "" ? null : `http://${process.env.SERVER}:${process.env.PORT}/${process.env.GIFT_PATH}${value.image}`,
          description: value.description,
          createdAt: moment(value.created_at).format("YYYY-MM-DD HH:mm:ss"),
          updatedAt:
            value.updated_at == null
              ? null
              : moment(value.updated_at).format("YYYY-MM-DD HH:mm:ss"),
        });
      }
      return res.json({
        success: true,
        data: data,
      });
    } else {
      return res.status(422).json({
        success: false,
        message: "data not found",
      });
    }
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// create new gift
exports.create = async (res, data, next) => {
  try {
    // check if username already exists
    const [queryCheck] = await conn.query(
      "SELECT `code` FROM `tbl_gift` WHERE `code`=?",
      [data.code]
    );
    if (queryCheck.length > 0) {
      return res.status(409).json({
        success: false,
        message:
          "redeem code has been used to another item, please use another code",
      });
    }

    // create gift path if not exists
    const path = `public/${process.env.GIFT_PATH}`;
    let fileName = crypto.randomBytes(8).toString("hex");
    const optionalObj = { fileName: fileName, type: "png" };
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true });
    }

    // if username is not exist, then create new user
    const [create] = await conn.query(
      "INSERT INTO `tbl_gift`(`code`, `name`, `color`, `ram`, `storage`, `stock`, `point`, `status`, `description`, `image`)  VALUES (?,?,?,?,?,?,?,?,?,?)",
      [
        data.code,
        data.name,
        data.color,
        data.ram,
        data.storage,
        data.stock,
        data.point,
        data.status,
        data.description,
        data.image == null ? null : `${fileName}.png`,
      ]
    );
    if (create.affectedRows === 1) {
      // if data is created, then upload image
      base64ToImage(data.image, path, optionalObj);
      return res.status(201).json({
        success: true,
        message: "gift saved successfully",
      });
    }
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// update gift data
exports.update = async (res, id, data, next) => {
  const path = `public/${process.env.GIFT_PATH}`;
  try {
    // check if data exists
    const [queryCheck] = await conn.query(
      "SELECT `id`, `image` FROM `tbl_gift` WHERE `id`=?",
      [id]
    );
    if (queryCheck.length === 0) {
      return res.status(422).json({
        success: false,
        message: "data not found",
      });
    }

    // if data exist, update data
    const oldFile = `${path}${queryCheck[0].image}`;
    let fileName = crypto.randomBytes(8).toString("hex");
    if (data.image != null) {
      // create gift path if not exists
      const optionalObj = { fileName: fileName, type: "png" };
      if (!fs.existsSync(path)) {
        fs.mkdirSync(path, { recursive: true });
      }
      base64ToImage(data.image, path, optionalObj);
      data["image"] = `${fileName}.png`;
    }
    await conn
      .query("UPDATE `tbl_gift` SET ? WHERE `id` = ?", [data, id])
      .then((_) => {
        // if image is updated, then delete old image
        if (data.image != null && queryCheck[0].image != "") {
          if (fs.existsSync(oldFile)) {
            fs.unlinkSync(oldFile);
          }
        }
        return res.status(202).json({
          success: true,
          message: "gift data updated successfully",
        });
      });
  } catch (error) {
    // if error and image not null, then delete the uploaded image
    if (data.image != null) {
      const newFile = `${path}${fileName}.png`;
      if (fs.existsSync(newFile)) {
        fs.unlinkSync(newFile);
      }
    }
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// update git attribute
exports.updateAttribute = async (res, id, data, next) => {
  try {
    // check if data exists
    const [queryCheck] = await conn.query(
      "SELECT `id` FROM `tbl_gift` WHERE `id`=?",
      [id]
    );
    if (queryCheck.length === 0) {
      return res.status(422).json({
        success: false,
        message: "data not found",
      });
    }

    // if data exist, update data
    await conn
      .query("UPDATE `tbl_gift` SET ? WHERE `id` = ?", [data, id])
      .then((_) => {
        return res.status(202).json({
          success: true,
          message: "gift data updated successfully",
        });
      });
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// delete gift data
exports.delete = async (res, id, next) => {
  try {
    // check if data exists
    const [queryCheck] = await conn.query(
      "SELECT `id` FROM `tbl_gift` WHERE `id`=?",
      [id]
    );
    if (queryCheck.length === 0) {
      return res.status(422).json({
        success: false,
        message: "data not found",
      });
    }

    // if data exist, delete data
    await conn
      .query("DELETE FROM `tbl_gift` WHERE `id` = ?", id)
      .then((result) => {
        return res.status(202).json({
          success: true,
          message: "gift data was deleted",
        });
      });
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// redeem gift
exports.redeem = async (res, data, next) => {
  try {
    for (const redeemCode of data.redeemCode) {
      // get item by redeem code
      const [item] = await conn.query(
        "SELECT * FROM `tbl_gift` WHERE `code`= ?",
        [redeemCode]
      );
      if (item.length === 0) {
        return res.status(422).json({
          success: false,
          message: "item not found",
        });
      }

      // check if user is exists
      const [checkUser] = await conn.query(
        "SELECT `id`, `name` FROM `tbl_user` WHERE `id`= ?",
        [data.userID]
      );
      if (checkUser.length === 0) {
        return res.status(422).json({
          success: false,
          message: "user not found",
        });
      }

      // if user exist, count user point
      const [plus] = await conn.query(
        "SELECT SUM(point) AS point FROM `tbl_point` WHERE user_id = ? AND gift_id IS NULL",
        [data.userID]
      );
      const [min] = await conn.query(
        "SELECT SUM(point) AS point FROM `tbl_point` WHERE user_id = ? AND gift_id IS NOT NULL",
        [data.userID]
      );
      if (plus[0].point - min[0].point < item[0].point) {
        return res.status(422).json({
          success: false,
          message: "not enough point",
        });
      }

      // check stock
      const [stock] = await conn.query(
        "SELECT * FROM `tbl_redeem` WHERE `gift_id`= ?",
        [item[0].id]
      );
      if (item.length > 0) {
        if (stock.length >= item[0].stock) {
          // out of stock
          return res.status(422).json({
            success: false,
            message: "gift is out of stock",
          });
        } else {
          const [createRedeem] = await conn.query(
            "INSERT INTO `tbl_redeem`(`user_id`, `gift_id`)  VALUES (?,?)",
            [data.userID, item[0].id]
          );
          const [createPoint] = await conn.query(
            "INSERT INTO `tbl_point`(`user_id`,`point`, `gift_id`) VALUES(?,?,?)",
            [data.userID, item[0].point, item[0].id]
          );
          if (
            createRedeem.affectedRows === 1 &&
            createPoint.affectedRows === 1 &&
            data.redeemCode[data.redeemCode.length - 1] === redeemCode
          ) {
            return res.status(201).json({
              success: true,
              message: "redeem success",
            });
          }
        }
      } else {
        return res.status(422).json({
          success: false,
          message: "data not found",
        });
      }
    }
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// find all redeem by user id
exports.redeemByUser = async (res, id, next) => {
  try {
    // count user point
    const [plus] = await conn.query(
      "SELECT SUM(point) AS point FROM `tbl_point` WHERE user_id = ? AND 	gift_id IS NULL",
      [id]
    );
    const [min] = await conn.query(
      "SELECT SUM(point) AS point FROM `tbl_point` WHERE user_id = ? AND 	gift_id IS NOT NULL",
      [id]
    );

    let userPoint = 0;
    if (plus != null || min != null) {
      userPoint = plus[0].point;
    }

    const [query] = await conn.query(
      "SELECT a.id as redeemID, a.created_at as redeem_at, b.* FROM tbl_redeem as a LEFT JOIN tbl_gift as b ON ( a.gift_id = b.id ) WHERE a.user_id = ?",
      [id]
    );

    if (query.length > 0) {
      // change date formate from 2022-03-03T15:15:45.000Z to 2022-03-03 15:15:45
      let data = [];
      let pointUsed = 0;
      for (const value of query) {
        pointUsed += value.point;
        data.push({
          id: value.redeemID,
          code: value.code,
          name: value.name,
          color: value.color,
          ram: value.ram,
          storage: value.storage,
          point: value.point,
          description: value.description,
          redeemedAt: moment(value.redeem_at).format("YYYY-MM-DD HH:mm:ss"),
        });
      }
      let pointRemain = userPoint - pointUsed;
      return res.json({
        success: true,
        totalPoint: parseInt(userPoint),
        pointUsed: pointUsed,
        pointRemain: pointRemain,
        redeemedItem: data,
      });
    } else {
      return res.status(422).json({
        success: false,
        message: "this user never redeem any gift",
      });
    }
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// rated the redeemed gift
exports.rating = async (res, data, next) => {
  try {
    // check if redeem is redeemed by the user
    const [queryCheck] = await conn.query(
      "SELECT `id` FROM `tbl_redeem` WHERE `id`=? AND `user_id`=?",
      [data.redeemID, data.userID]
    );
    if (queryCheck.length === 0) {
      return res.status(422).json({
        success: false,
        message: "data not found",
      });
    }

    // check if redeem has been rated by the user
    const [queryCheck2] = await conn.query(
      "SELECT a.id FROM tbl_rating as a LEFT JOIN tbl_redeem as b ON ( a.redeem_id = b.id ) WHERE a.redeem_id = ? AND b.user_id = ?",
      [data.redeemID, data.userID]
    );
    if (queryCheck2.length > 0) {
      return res.status(409).json({
        success: false,
        message: "this redeem has been rated this user",
      });
    }

    // if redeem is exist, save to table rating
    const [create] = await conn.query(
      "INSERT INTO `tbl_rating`(`redeem_id`, `rating`)  VALUES (?,?)",
      [data.redeemID, data.rating]
    );
    if (create.affectedRows === 1) {
      return res.status(201).json({
        success: true,
        message: "successfully rated redeemed gift",
      });
    }
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};
