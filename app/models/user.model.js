require("dotenv").config();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const conn = require("../config/db.config").promise();

// create/register user
exports.register = async (res, data, next) => {
  try {
    // check if username already exists
    const [queryCheck] = await conn.query(
      "SELECT `username` FROM `tbl_user` WHERE `username`=?",
      [data.username]
    );
    if (queryCheck.length > 0) {
      return res.status(409).json({
        success: false,
        message: "username has taken, please use another username",
      });
    }

    // if username does not exist, create user
    const passwordHash = await bcrypt.hash(data.password, 12);
    var sql, value;
    if (data.role == null) {
      // if role is null, then set role to 3 (user)
      sql =
        "INSERT INTO `tbl_user`(`name`,`username`,`password`) VALUES(?,?,?)";
      value = [data.name, data.username, passwordHash];
    } else {
      // check if role is admin
      const token = data.authorization;
      const decoded = jwt.verify(token, process.env.JWTSECRETKEY);
      if (decoded.role != "admin") {
        return res.status(403).send({
          status: "error",
          message:
            "access denied, you don't have permission to access this API",
        });
      }
      sql =
        "INSERT INTO `tbl_user`(`name`,`username`, `role`, `password`) VALUES(?,?,?,?)";
      value = [data.name, data.username, data.role, passwordHash];
    }
    const [queryRegister] = await conn.query(sql, value);
    if (queryRegister.affectedRows === 1) {
      return res.status(201).json({
        success: true,
        message: "user registration success",
      });
    }
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// login to get access token
exports.login = async (res, data, next) => {
  try {
    // find user by username
    const [query] = await conn.query(
      "SELECT * FROM `tbl_user` WHERE `username`=?",
      [data.username]
    );
    if (query.length === 0) {
      return res.status(422).json({
        success: false,
        message: "User not found",
      });
    }

    // if user found, then compare password
    const passwordMatch = await bcrypt.compare(
      data.password,
      query[0].password
    );
    if (!passwordMatch) {
      return res.status(401).json({
        success: false,
        message: "incorrect password",
      });
    }

    // if password match, then create token
    const token = jwt.sign(
      { id: query[0].id, role: query[0].role },
      process.env.JWTSECRETKEY,
      { algorithm: "HS384", expiresIn: "1h" }
    );
    return res.json({
      success: true,
      token: token,
    });
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// get user data based on token used
exports.userData = async (res, data, next) => {
  try {
    // get user by token
    const token = data.authorization.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWTSECRETKEY);
    const [row] = await conn.query(
      "SELECT `id`,`name`,`username`, `created_at` FROM `tbl_user` WHERE `id`=?",
      [decoded.id]
    );

    if (row.length > 0) {
      return res.json({
        success: true,
        data: row[0],
      });
    } else {
      res.status(422).json({
        success: false,
        message: "user not found",
      });
    }
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// view all user data (admin only)
exports.allUser = async (res, next) => {
  try {
    // if token is valid, then get user by token
    const [query] = await conn.query("SELECT * FROM `tbl_user`");

    if (query.length > 0) {
      // change date formate from 2022-03-03T15:15:45.000Z to 2022-03-03 15:15:45
      var data = [];
      for (const value of query) {
        data.push({
          id: value.id,
          name: value.name,
          username: value.username,
          createdAt: moment(value.created_at).format("YYYY-MM-DD HH:mm:ss"),
          updatedAt:
            value.updated_at == null
              ? null
              : moment(value.updated_at).format("YYYY-MM-DD HH:mm:ss"),
        });
      }
      return res.json({
        success: true,
        data: data,
      });
    } else {
      res.status(422).json({
        success: false,
        message: "user not found",
      });
    }
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// update data user like change name or username
exports.update = async (res, id, data, next) => {
  try {
    // check if user exists
    const [queryCheck1] = await conn.query(
      "SELECT `username` FROM `tbl_user` WHERE `id`=?",
      [id]
    );
    if (queryCheck1.length === 0) {
      return res.status(422).json({
        success: false,
        message: "user not found",
      });
    }

    // check if username already exists
    if (data.username != null && queryCheck1[0].username != data.username) {
      const [queryCheck2] = await conn.query(
        "SELECT `username` FROM `tbl_user` WHERE `username`=?",
        [data.username]
      );
      if (queryCheck2.length > 0) {
        return res.status(409).json({
          success: false,
          message: "username has taken, please use another username",
        });
      }
    }

    // if user exist, update user
    await conn
      .query("UPDATE `tbl_user` SET ? WHERE `id` = ?", [data, id])
      .then((_) => {
        return res.status(202).json({
          success: true,
          message: "user data updated successfully",
        });
      });
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// update data user
exports.changePassword = async (res, id, data, next) => {
  try {
    // find user by id
    const [query] = await conn.query("SELECT * FROM `tbl_user` WHERE `id`=?", [
      id,
    ]);
    if (query.length === 0) {
      return res.status(422).json({
        success: false,
        message: "User not found",
      });
    }

    // if user found, then compare password
    const passwordMatch = await bcrypt.compare(data.old, query[0].password);
    if (!passwordMatch) {
      return res.status(401).json({
        success: false,
        message: "old incorrect password",
      });
    }

    // if password match, then update password
    const passwordHash = await bcrypt.hash(data.new, 12);
    await conn
      .query("UPDATE `tbl_user` SET ? WHERE `id` = ?", [
        { password: passwordHash },
        id,
      ])
      .then((_) => {
        return res.status(202).json({
          success: true,
          message: "password changed successfully",
        });
      });
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};

// delete data user
exports.delete = async (res, id, next) => {
  try {
    // check if user exists
    const [queryCheck] = await conn.query(
      "SELECT `username` FROM `tbl_user` WHERE `id`=?",
      [id]
    );
    if (queryCheck.length === 0) {
      return res.status(422).json({
        success: false,
        message: "user not found",
      });
    }

    // if user exist, delete user
    await conn
      .query("DELETE FROM `tbl_user` WHERE `id` = ?", id)
      .then((result) => {
        return res.status(202).json({
          success: true,
          message: "user data was deleted",
        });
      });
  } catch (error) {
    next(error);
    return res.status(500).json({
      success: false,
      message: "an error occured, please try again later",
    });
  }
};
