require("dotenv").config();
const mysql = require("mysql2");

let host = process.env.DB_HOST;
let port = process.env.DB_PORT || 3306;
const connection = mysql.createPool({
  host: host,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  waitForConnections: true,
  connectionLimit: 100,
  queueLimit: 0,
});

connection.getConnection(function (err, conn) {
  // throw error if connection failed
  if (err) throw err;

    // if connection successful
  console.log(`Database connected to server ${host}:${port}`);

  // release the connection when finished
  connection.releaseConnection(conn);
});

module.exports = connection;
