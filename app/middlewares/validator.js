const { body, validationResult } = require("express-validator");

let register = [
  body("name", "the length of the name must be at least 3 characters").escape().trim().isLength({ min: 3 }),
  body("username", "invalid username").notEmpty().trim(),
  body("password", "password must be at least 4 characters").trim().isLength({ min: 4 }),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        success: false,
        message: "invalid input",
        errors: errors.array(),
      });
    }
    next();
  },
];

let login = [
  body("username", "invalid username").notEmpty().trim(),
  body("password", "password cannot be empty").notEmpty().trim(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        success: false,
        message: "invalid input",
        errors: errors.array(),
      });
    }
    next();
  },
];

let point = [
  body("point", "point cannot be empty").notEmpty().trim().isNumeric().withMessage("point must be numeric"),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        success: false,
        message: "invalid input",
        errors: errors.array(),
      });
    }
    next();
  },
];

let gift = [
  body("code", "the length of the name must be at least 10 characters").escape().trim().isLength({ min: 10 }).isAlphanumeric().withMessage("code must be alphanumeric"),
  body("name", "the length of the name must be at least 3 characters").escape().trim().isLength({ min: 3 }),
  body("description", "the length of the description must be at least 10 characters").escape().trim().isLength({ min: 10 }),
  body("color", "color cannot be empty").notEmpty().trim(),
  body("ram", "ram cannot be empty").notEmpty().trim().isNumeric().withMessage("ram must be numeric"),
  body("storage", "storage cannot be empty").notEmpty().trim().isNumeric().withMessage("storage must be numeric"),
  body("stock", "stock cannot be empty").notEmpty().trim().isNumeric().withMessage("stock must be numeric"),
  body("point", "point cannot be empty").notEmpty().trim().isNumeric().withMessage("point must be numeric"),
  body("image", "image cannot be empty").notEmpty().trim(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        success: false,
        message: "invalid input",
        errors: errors.array(),
      });
    }
    next();
  },
];

let redeem = [
  body("redeemCode", "redeemCode cannot be empty").notEmpty().isArray().withMessage("redeemCode must be an array"),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        success: false,
        message: "invalid input",
        errors: errors.array(),
      });
    }
    next();
  },
];

let rating = [
  body("redeemID", "redeemID cannot be empty").notEmpty().trim().isNumeric().withMessage("redeemID must be numeric"),
  body("rating", "rating cannot be empty").notEmpty().trim().isFloat({ min:0.0, max: 5.0}).withMessage("rating must be numeric and between 0 and 5"),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        success: false,
        message: "invalid input",
        errors: errors.array(),
      });
    }
    next();
  },
];

module.exports = { register, login, point, gift, redeem, rating };
