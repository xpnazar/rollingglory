require("dotenv").config();
const jwt = require("jsonwebtoken");

module.exports = function (role) {
  return (req, res, next) => {
    const authHeader = req.headers.authorization.split(" ")[1];

    if (!authHeader) {
      return res.status(403).send({
        status: "error",
        message: "token required for authentication",
      });
    }

    jwt.verify(authHeader, process.env.JWTSECRETKEY, function (err, decoded) {
      if (err) {
        // check if jwt token was expired or invalid
        let expired = err.message == "jwt expired";
        return res.status(403).send({
          status: "error",
          message: expired ? "expired token" : "invalid token",
        });
      }

      if (role.includes(decoded.role) || role.includes("allRole")) {
        return next();
      } else {
        return res.status(403).send({
          status: "error",
          message:
            "access denied, you don't have permission to access this API",
        });
      }
    });
  };
};
