const router = require("express").Router();
const auth = require("../middlewares/authorization");
const users = require("../controllers/user.controller");
const validator = require("../middlewares/validator");

router.post("/register", validator.register, users.register);
router.post("/login", validator.login, users.login);
router.get("/userData", auth("allRole"), users.userData);
router.get("/user", auth("admin"), users.allUser);
router.put("/user/:id", auth("admin"), users.update);
router.patch("/user/:id", auth("allRole"), users.changePassword);
router.delete("/user/:id", auth("admin"), users.delete);

module.exports = router;
