const router = require("express").Router();
const pjson = require("../../package.json");
const user = require("./user.routes");
const point = require("./point.routes");
const gift = require("./gift.routes");

// set init response
router.get("/", (req, res) => {
  res.json({
    author: pjson.author,
    name: pjson.name,
    description: pjson.description,
    version: pjson.version,
    license: pjson.license,
  });
});

// api router
router.use("/api", [user, point, gift]);

module.exports = router;
