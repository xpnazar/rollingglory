const router = require("express").Router();
const auth = require("../middlewares/authorization");
const gifts = require("../controllers/gift.controller");
const validator = require("../middlewares/validator");

router.get("/gifts", auth("allRole"), gifts.findAll);
router.get("/gifts/:id", auth("allRole"), gifts.findOne);
router.post("/gifts", auth(["admin", "operator"]), validator.gift, gifts.create);
router.put("/gifts/:id", auth(["admin", "operator"]), gifts.update);
router.patch("/gifts/:id", auth(["admin", "operator"]), gifts.updateAttribute);
router.delete("/gifts/:id", auth(["admin", "operator"]), gifts.delete);
router.post("/gifts/:id/redeem", auth("allRole"), validator.redeem, gifts.redeem);
router.get("/gifts/redeem/:id", auth("allRole"), gifts.redeemByUser);
router.post("/gifts/:id/rating", auth("allRole"), validator.rating, gifts.rating);

module.exports = router;
