const router = require("express").Router();
const auth = require("../middlewares/authorization");
const points = require("../controllers/point.controller");
const validator = require("../middlewares/validator");

router.get("/point/:id", auth("allRole"), points.check);
router.post("/point", auth(["admin", "operator"]), validator.point, points.create);

module.exports = router;
