-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Mar 05, 2022 at 01:13 PM
-- Server version: 5.7.34
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rollingglory`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gift`
--

CREATE TABLE `tbl_gift` (
  `id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `color` varchar(50) NOT NULL,
  `ram` int(11) NOT NULL,
  `storage` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `point` int(11) NOT NULL,
  `status` int(1) DEFAULT NULL COMMENT '1: Hot Items, 2: New, 3. Best Seller',
  `image` text NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_gift`
--

INSERT INTO `tbl_gift` (`id`, `code`, `name`, `color`, `ram`, `storage`, `stock`, `point`, `status`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'GIFT042022', 'Samsung Galaxy S9', 'Midnight Black', 4, 64, 5, 200000, 3, '1623312926bcfa09.png', 'Ukuran layar: 6.2 inci, Dual Edge Super AMOLED 2960 x 1440 (Quad HD+) 529 ppi, 18.5:9 Memori: RAM 6 GB (LPDDR4), ROM 64 GB, MicroSD up to 400GB Sistem operasi: Android 8.0 (Oreo) CPU: Exynos 9810 Octa-core (2.7GHz Quad + 1.7GHz Quad), 64 bit, 10nm processor Kamera: Super Speed Dual Pixel, 12 MP OIS (F1.5&#x2F;F2.4 Dual Aperture) + 12MP OIS (F2.4) with LED flash, depan 8 MP, f&#x2F;1.7, autofocus, 1440p@30fps, dual video call, Auto HDR SIM: Dual SIM (Nano-SIM) Baterai: Non-removable Li-Ion 3500 mAh , Fast Charging on wired and wireless', '2022-03-04 21:31:23', '2022-03-05 17:28:13'),
(2, 'GIFT052022', 'iPhone 13 Pro Max', 'Graphite Gray', 6, 512, 5, 500000, 1, '38c1b0e81d938aac.png', 'Apple iPhone 13 Pro Max merupakan handphone HP dengan kapasitas 3285mAh dan layar 6.7 inch yang dilengkapi dengan kamera belakang 12 + 12 + 12MP dengan tingkat densitas piksel sebesar 458ppi dan tampilan resolusi sebesar 1284 x 2778pixels. Dengan berat sebesar 240g, handphone HP ini memiliki prosesor Hexa Core.', '2022-03-04 22:26:48', '2022-03-05 13:25:05'),
(3, 'GIFT032022', 'Huawei P50 Pro', 'Golden Black', 8, 256, 5, 300000, 2, 'fb50dcbde205598f.png', 'HUAWEI P50 Pro merupakan handphone HP dengan kapasitas 4360mAh dan layar 6.6 inch yang dilengkapi dengan kamera belakang 50 + 64 + 13 + 40MP dengan tingkat densitas piksel sebesar 450ppi dan tampilan resolusi sebesar 1228 x 2700pixels. Dengan berat sebesar 195g, handphone HP ini memiliki prosesor Octa Core.', '2022-03-05 13:01:05', NULL),
(4, 'GIFT012022', 'Samsung Galaxy S22 Ultra', 'Phanton Black', 12, 512, 5, 500000, 2, '58b1950500427559.png', 'Samsung Galaxy S22 Ultra merupakan handphone HP dengan kapasitas 5000mAh dan layar 6.8 inch yang dilengkapi dengan kamera belakang 12 + 108 + 10 + 10MP dengan tingkat densitas piksel sebesar 500ppi dan tampilan resolusi sebesar 3088 x 1440pixels. Dengan berat sebesar 228g, handphone HP ini memiliki prosesor Octa Core', '2022-03-05 17:19:14', NULL),
(5, 'GIFT022022', 'iPhone 12 Pro Max', 'Red', 6, 256, 5, 400000, 2, '7a4406473a8655c1.png', 'iPhone 12 Pro Max merupakan handphone HP dengan kapasitas 2815mAh dan layar 6.1 inch yang dilengkapi dengan kamera belakang 12 + 12MP dengan tingkat densitas piksel sebesar 476ppi dan tampilan resolusi sebesar 2340 x 1080pixels. Dengan berat sebesar 162g, handphone HP ini memiliki prosesor Octa Core', '2022-03-05 17:22:49', NULL),
(6, 'GIFT062022', 'Samsung Galaxy Z Flip', 'Mirror Gold', 8, 256, 5, 750000, 1, '98cf37ad65805bbf.png', 'Samsung Galaxy Z Flip merupakan handphone HP dengan kapasitas 3300mAh dan layar 6.7 inch yang dilengkapi dengan kamera belakang 12 + 12MP dengan tingkat densitas piksel sebesar 425ppi dan tampilan resolusi sebesar 1080 x 2636pixels. Dengan berat sebesar 183g, handphone HP ini memiliki prosesor Octa Core.', '2022-03-05 17:31:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_point`
--

CREATE TABLE `tbl_point` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gift_id` int(11) DEFAULT NULL COMMENT 'if null that mean plus point',
  `point` bigint(30) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_point`
--

INSERT INTO `tbl_point` (`id`, `user_id`, `gift_id`, `point`, `created_at`) VALUES
(1, 3, NULL, 1000000, '2022-03-04 20:40:45'),
(2, 3, NULL, 100000, '2022-03-04 20:57:55'),
(3, 3, NULL, 500000, '2022-03-04 20:58:06'),
(8, 3, NULL, 100000, '2022-03-04 23:23:15'),
(9, 3, 2, 500000, '2022-03-04 23:27:03'),
(11, 3, 1, 200000, '2022-03-04 23:36:43'),
(12, 3, 3, 300000, '2022-03-05 14:38:14'),
(13, 3, 1, 200000, '2022-03-05 14:38:14'),
(14, 4, NULL, 500000, '2022-03-05 14:44:42'),
(15, 4, 2, 500000, '2022-03-05 14:45:12'),
(16, 3, 2, 500000, '2022-03-05 19:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating`
--

CREATE TABLE `tbl_rating` (
  `id` int(11) NOT NULL,
  `redeem_id` int(11) NOT NULL,
  `rating` float NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_rating`
--

INSERT INTO `tbl_rating` (`id`, `redeem_id`, `rating`, `created_at`) VALUES
(1, 2, 5, '2022-03-05 10:52:18'),
(2, 5, 3, '2022-03-05 14:45:45'),
(3, 1, 4.5, '2022-03-05 14:49:22'),
(4, 4, 4, '2022-03-05 14:51:59'),
(5, 6, 3.5, '2022-03-05 19:57:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_redeem`
--

CREATE TABLE `tbl_redeem` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gift_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_redeem`
--

INSERT INTO `tbl_redeem` (`id`, `user_id`, `gift_id`, `created_at`) VALUES
(1, 3, 2, '2022-03-04 23:27:03'),
(2, 3, 1, '2022-03-04 23:36:43'),
(3, 3, 3, '2022-03-05 14:38:14'),
(4, 3, 1, '2022-03-05 14:38:14'),
(5, 4, 2, '2022-03-05 14:45:12'),
(6, 3, 2, '2022-03-05 19:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `role` varchar(10) NOT NULL DEFAULT 'user' COMMENT 'admin, operator, user',
  `username` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `role`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'admin', '$2a$12$YZVdt1pY3/3sDoKA.jGpJuHqie/xZ8j3vBwi.KGVvHM2mwWBTsQvm', '2022-03-03 22:15:45', NULL),
(2, 'Operator', 'operator', 'operator', '$2a$12$Bk849WEBeuF/0Yly6Hu3YuMSDOXfEEA2V413D8sK8hN.cOHUlNzfC', '2022-03-04 00:15:26', NULL),
(3, 'User', 'user', 'user', '$2a$12$qTHcdG2XOmP.X6KAcB6HLeoFRjhNV14nTCmGcBkuzosNjgKcVmFee', '2022-03-04 20:37:19', '2022-03-04 20:38:53'),
(4, 'User 2', 'user', 'user2', '$2a$12$8k7OyrGfUJLqlTA3lPlvtOHCjGNe9AtCT9ylYYEN4EvoHnUlx.J26', '2022-03-05 14:42:57', NULL),
(5, 'User 3', 'user', 'user3', '$2a$12$/m1JtuYFJCO8sJ6iPLKOw.N9efdO/Yiz/3xq1RYjPGIrg8hnNHWdC', '2022-03-05 19:11:02', NULL),
(6, 'User 4', 'user', 'user4', '$2a$12$5nIaRcPe/X3eFKEAeafJLe7AXx72JC5yid82RCqNA1SA50O3jMlLW', '2022-03-05 19:11:30', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_gift`
--
ALTER TABLE `tbl_gift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_point`
--
ALTER TABLE `tbl_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_rating`
--
ALTER TABLE `tbl_rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `redeem` (`redeem_id`);

--
-- Indexes for table `tbl_redeem`
--
ALTER TABLE `tbl_redeem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user_id`),
  ADD KEY `gift` (`gift_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_gift`
--
ALTER TABLE `tbl_gift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_point`
--
ALTER TABLE `tbl_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_rating`
--
ALTER TABLE `tbl_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_redeem`
--
ALTER TABLE `tbl_redeem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_rating`
--
ALTER TABLE `tbl_rating`
  ADD CONSTRAINT `redeem` FOREIGN KEY (`redeem_id`) REFERENCES `tbl_redeem` (`id`);

--
-- Constraints for table `tbl_redeem`
--
ALTER TABLE `tbl_redeem`
  ADD CONSTRAINT `gift` FOREIGN KEY (`gift_id`) REFERENCES `tbl_gift` (`id`),
  ADD CONSTRAINT `user` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
