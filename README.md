# Backned Developer Test #

This project was created for backend developer testing of [Rolling Glory](http://rollingglory.com/) and has been deployed to [Heroku Cloud Platfrom](https://www.heroku.com/)
which can be accessed via the following url: [https://rollingglory.herokuapp.com/](https://rollingglory.herokuapp.com/)

## Tech specifications ##

* Framework : [ExpressJS](https://expressjs.com/)
* Database : [MySQL](https://www.mysql.com/)

## Installation ##

1. Clone this repository
2. Get the project database file named `rollingglory.sql` from the docs folder and import it into phpMyAdmin
3. Set `.env` based on `.env.example` file
4. Then run `npm install`

## How to run the server locally ##

1. Make sure all the installation step was complete, then run `npm start` or `npm run dev`
2. Access `http://localhost:3000/` from web browser, postman, insomnia, etc
3. If there are no errors, a display like this will appear  
![sucsess response](https://user-images.githubusercontent.com/56527536/156878876-71bce3fe-e7ef-477b-ac05-6811119ffaa6.png)

## Library used ##

* [base64-to-image](https://www.npmjs.com/package/base64-to-image) - Decode base64 to image and save the image to disk
* [bcryptjs](https://www.npmjs.com/package/bcryptjs) - Compare and hash passwords
* [cors](https://www.npmjs.com/package/cors) - CORS middleware
* [dotenv](https://www.npmjs.com/package/dotenv) - Environment variables
* [express](https://www.npmjs.com/package/express) - Framework
* [express-validator](https://www.npmjs.com/package/express-validator) - Middleware library to validate input data
* [helmet](https://www.npmjs.com/package/helmet) - Secure your Express apps by setting various HTTP headers
* [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken) - Generate and validate jsonwebtoken
* [moment](https://www.npmjs.com/package/moment) - Parsing, validating, manipulating, and formatting dates
* [morgan](https://www.npmjs.com/package/morgan) - HTTP request logger
* [mysql2](https://www.npmjs.com/package/mysql2) - MySQL client
* [nodemon](https://www.npmjs.com/package/nodemon) - To automatically restart the server when a changes made

## API endpoint ##

### baseURL = `http://localhost:3000/api/` for locally or `https://rollingglory.herokuapp.com/api` for remote server ###

### Use this endpoint by combining with baseURL. Example for `/login` : `http://localhost:3000/api/login` ####

### Username & password ###

1. The password for admin = `username: admin`, `password: admin`
2. The password for operator = `username: operator`, `password: operator`
3. The password for user = `username: user`, `password: user` or `username: user2`, `password: user2`

### Step to access endpoint ###

1. Login using `/login` endpoint, then when login success you will get token
2. Go to main folder (Rolling Glory folder), click on theh authorization tab
3. At the authorization type choose `Bearer Token` and paste your previous login token to the `token` field
4. The token will expire in 1 hour  
![set token](https://user-images.githubusercontent.com/56527536/156881992-9c9d22ab-c74c-433f-bc79-f42b50e0802e.png)

### Endpoint list ###

URL | Method | Body | Params | QueryString | Explanation
---|---|---|---|---|---
`/login`| POST | `{"username": "your username", "password": "your password"}` | - | - | Login and get access token, `no token required`
`/register`| POST | `{"name": "your name", "username": "username (unique)", "role": "admin/operator/user", "password": "min. 4 character"}` | - | - | Register new user role (no token required) and create new user with specific role (admin role, `required token`)
`/userData`|GET| - | - | - | Get current user data based on used token (all role, `required token`)
`/allUser`|GET| - | - | - | Get all user data (admin role, `required token`)
`/user`|PUT| `{"name": "User", "username": "user"}` | - | `/:user_id` | Change name and username (admin role, `required token`)
`/user`|PATCH| `{"old": "old password", "new": "new password"}` | - | `/:user_id` | Change user password (all role, `required token`)
`/user`|DELETE| - | - | `/:user_id` | Delete user data by `id` (admin role, `required token`)
`/point`|GET| - | - | `/:user_id` | Get current user point (all role, `required token`)
`/point`|POST| `{"user_id": "user id", "point": <integer>}` | - | - | Add user point (admin & operator role, `required token`)
`/gifts`|GET| - | `?order=<asc/desc> (order data by ascending or descending)&orderBy=<rating/date> (date to sort by created time, rating to sort by rating)&page=<integer> (request page parameter)&perPage=<integer> (total data per-page)` | - | Get all gift data (all role, `required token`)
`/gifts`|GET| - | - | `/:gift_id` | Get single gift by `id` (admin & operator role, `required token`)
`/gifts`|POST| `{"code": "redeem code", "name": "gift name", "color": "gift color", "ram": <integer>, "storage": <integer>, "stock": <integer>, "point": <integer>, "status": "1: Hot Items, 2: New, 3. Best Seller"<integer>, "description": "gift description", "image": "gift image"<base64>}` | - | - | Create new gift data (admin & operator role, `required token`)
`/gifts`|PUT| `can use all or some input that used in /gift POST endpoint` | - | `/:gift_id` | Change gift data (admin & operator role, `required token`)
`/gifts`|PATCH| `use on off input that used in /gift POST endpoint, ex: {"code": "new redeem code"}` | - | `/:gift_id` | Change gift data attribute (admin & operator role, `required token`)
`/gifts`|DELETE| - | - | `/:gift_id` | Delete gift data by `id` (admin & operator role, `required token`)
`/gifts/:user_id/redeem`|POST| `{"redeemCode": ["redeem code", "redeem code"}` | - | `/:user_id` | Redeem multiple gift if point was enough (all role, `required token`)
`/gifts/redeem`|GET| - | - | `/:user_id` | Check redeemed item by `user_id` (all role, `required token`)
`/gifts/:user_id/rating`|POST| `{"redeemID": <integer>, "rating": <float>}` | - | `/:user_id` | Rate items that have been redeemed, only 1 rating/user/redeemed item (all role, `required token`)

## Need something to discuss? Reach me via ##

* [LinkedIn](https://www.linkedin.com/in/nazarudin/)
* [WhatsApp](https://wa.me/6281365041803)
